import 'dotenv/config';
import { WebSocket } from 'ws';
import Protobuf from 'protobufjs';
import { cleanEnv, str } from 'envalid';

const envs = cleanEnv(process.env, {
  YAHOO_FINANCE_WSS: str(),
});

let pricingDataProto: Protobuf.Type;

Protobuf.load(`${__dirname}/pricing-data.proto`, (_error, root) => {
  if (typeof root !== 'undefined') {
    pricingDataProto = root.lookupType('yahooFinance.PricingData');
  }

  console.log(_error);
});

const yahooFinanceWebsocket = new WebSocket(envs.YAHOO_FINANCE_WSS);

yahooFinanceWebsocket.addEventListener('open', () => {
  console.log('Websocket connection established.');

  const message = JSON.stringify({
    subscribe: ['MSFT'],
  });

  console.log(message);

  yahooFinanceWebsocket.send(message);
});

yahooFinanceWebsocket.addEventListener('message', (event) => {
  console.log(new Date());
  // console.log(`Event type: ${event.type}`);
  // console.log(`Event target: ${JSON.stringify(event.target)}`);
  console.log(`Received new message from server: ${event.data}`);
  const decodedMessage = pricingDataProto.decode(Buffer.from(String(event.data), 'base64'));
  console.log(decodedMessage);
});
