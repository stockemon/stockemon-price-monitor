# stockemon-price-monitor

Standalone application to monitor stock and currency realtime price and update database records regularly.

## How to Run Server

### Commands
```bash
cd stockemon-price-monitor
npm ci
npm run dev # for development
npx tsc && npm run start # for production
```
